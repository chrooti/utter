# Utter

How many times did you just want to print something and instead had to bother with format strings, iostream and the likes? Utter is a single header output library that supports the 90% case where you just want to write a damn string to stdout and be done with it.

Caveats:
- POSIX only, but you can provide your own implementations.
- Floats not yet supported, but you can std::to_chars into a buffer.
- C++20 required

// TODO: asm test (+ register spilling)
// TODO: compile time test with lots of concats and coversion

## Docs

### Examples

- Basic usage

```cpp
utter::to(utter::STDOUT, S("Hello, world!\n"));
utter::to(utter::STDERR, S("Hello, "), S("world!\n"));
utter::to(33, S("Hello, "), S("world"), S("\n"));
```

- Constructing a buffer from various inputs

```cpp
// from a string
S("Hello world\n");

// from a number, compile time
S(16);

// from a number, compile time (preferred)
using namespace utter::literals;
S(16_str);

// from a number, at runtime
utter::as_dec(num);

// from a number, as hexadecimal, compile time
utter::as_hex<16>; // 0x16

// from a number, as hexadecimal, compile time, without prefix
utter::as_hex<16, UTTER_CONV_NO_PREFIX>(); // 0x10

// from a number, as binary, at runtime, filled to a length of six
utter::as_bin(16, 6); // 0b010000

// from a number, as binary, at runtime, filled to a length of six, without prefix
utter::as_bin(16, 6 | UTTER_CONV_NO_PREFIX); 010000
```

- Passing another type as buffer

```cpp
std::string string = "Hello, world!\n";
BufferView string_view = {.len = string->length(), .data = string->data()};
utter::to(utter::STDERR, string_view);
```

- Concatenating buffers

```cpp
utter::together("Hello, ", "world!\n");
```

- Repeating a character

```cpp
utter::repeat<5>('-');
```

### What does the S macro do?

It's a hack to force the string to have static storage duration, in order to avoid copies when the buffer is constexpr. In short: smaller assembly, better performance.

### Non-Linux targets

If you're compiling for a freestanding target or a target where utter can't find the writev function in the standard include path you can still use the library by defining/including the following struct and functions:
```
struct iovec {
    void *iov_base;
    size_t iov_len;
};

void* memcpy(void* destination, const void* source, size_t num);
ssize_t writev(int fd, const struct iovec* iovec, int count);
```

## Development

### Requirements
- clang 14
- ninja
- bash

### How to

```bash
./gen_ninja_config.sh
ninja test
```
(see file sources for options)
