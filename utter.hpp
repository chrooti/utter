#ifndef UTTER_H
#define UTTER_H

#include <stddef.h>
#include <stdint.h>

#include <concepts>
#include <utility>

/* OS macros */

#if __STDC_HOSTED__ && defined(__unix__) || (defined(__APPLE__) && defined(__MACH__))
#include <string.h>
#include <sys/uio.h>

#else
#if INTPTR_MAX == INT64_MAX
using ssize_t = int64_t;
#elif INTPTR_MAX == INT32_MAX
using ssize_t = int32_t;
#else
#error "Cannot detect pointer size"
#endif

struct iovec;
void* memset(void* destination, int value, size_t num);
void* memcpy(void* destination, const void* source, size_t num);
ssize_t writev(int fd, const struct iovec* iovec, int count);

#endif

/* Compiler macros */

namespace utter::detail {

#if defined(__clang__) || defined(__GNUC__)
#define UTTER_INLINE static inline __attribute__((__always_inline__, __unused__))

UTTER_INLINE constexpr unsigned int clz(size_t val) {
    return static_cast<unsigned int>(__builtin_clzll(val));
}

#else
#define UTTER_INLINE static inline

UTTER_INLINE constexpr unsigned int clz(size_t val);

#endif

} // namespace utter::detail

namespace utter {

/* Data structures */

template<size_t n>
struct Buffer {
    static constexpr size_t SIZE = n;

    size_t len;
    char data[n];
};

template<>
struct Buffer<0> {
    static constexpr size_t SIZE = 1;

    size_t len;
    char data[1];
};

struct BufferView {
    size_t len;
    const char* data;
};

} // namespace utter

/* Internal utilities */

namespace utter::detail {

template<size_t n>
static constexpr size_t strlen(const char (&)[n]) {
    return n - 1;
}

UTTER_INLINE constexpr unsigned int count_binary_digits(size_t val) {
    // NOTE: __builtin_clz(x) is undefined for x == 0
    return sizeof(size_t) * 8 - ::utter::detail::clz(val);
}

template<Buffer src, size_t i>
UTTER_INLINE constexpr char* small_inline_reverse_memcpy(char* dest) {
    if constexpr (i == 0) {
        return dest;
    } else {
        dest--;
        *dest = src.data[i - 1];
        return ::utter::detail::small_inline_reverse_memcpy<src, i - 1>(dest);
    }
}

template<Buffer src>
UTTER_INLINE constexpr char* small_inline_reverse_memcpy(char* dest) {
    return ::utter::detail::small_inline_reverse_memcpy<src, src.len>(dest);
}

template<typename T>
UTTER_INLINE constexpr T max(T a, T b) {
    return a > b ? a : b;
}

template<typename T>
UTTER_INLINE constexpr T min(T a, T b) {
    return a < b ? a : b;
}

} // namespace utter::detail

/* Buffer string constructors */

namespace utter::detail {

template<size_t... indices>
UTTER_INLINE constexpr Buffer<sizeof...(indices)>
to_buffer_impl(const char (&str)[sizeof...(indices) + 1], std::index_sequence<indices...>) {
    return {
        .len = sizeof...(indices),
        .data = {str[indices]...},
    };
}

template<auto val>
struct ToStatic {
    static constexpr decltype(val) STATIC_VAL = val;
};

} // namespace utter::detail

namespace utter {

template<size_t n>
UTTER_INLINE constexpr const Buffer<n>& to_buffer(const Buffer<n>& buf) {
    return buf;
}

UTTER_INLINE constexpr const BufferView& to_buffer(const BufferView& buf_view) {
    return buf_view;
}

UTTER_INLINE constexpr Buffer<1> to_buffer(const char chr) {
    return {
        .len = 1,
        .data = {chr},
    };
}

template<size_t n>
UTTER_INLINE constexpr Buffer<n - 1> to_buffer(const char (&str)[n]) {
    return ::utter::detail::to_buffer_impl(str, std::make_index_sequence<n - 1>{});
}

template<size_t n>
UTTER_INLINE constexpr BufferView to_buffer_view(const char (&data)[n]) {
    return {
        .len = n - 1,
        .data = data,
    };
}

UTTER_INLINE constexpr BufferView to_buffer_view(const char* data, size_t len) {
    return {
        .len = len,
        .data = data,
    };
}

} // namespace utter

#define S(...) ::utter::detail::ToStatic<::utter::to_buffer(__VA_ARGS__)>::STATIC_VAL

/* Literal */

namespace utter::literals {

template<char... chars>
UTTER_INLINE constexpr Buffer<sizeof...(chars)> operator""_str() {
    return {
        .len = sizeof...(chars),
        .data = {chars...},
    };
}

} // namespace utter::literals

/* Integer conversion */

namespace utter {

enum : int {
    // first 12 bits are reserved for fill size

    // add "0x", "0b" etc. prefix (ignored when printing decimal)
    CONV_NO_PREFIX = 0x1000,
};

} // namespace utter

namespace utter::detail {

// most architectures have 2 return registers in ABI, so these still fits

struct ConversionArgs {
    bool wants_prefix;
    unsigned int fill;
};

struct ConversionResult {
    char* buf_end;
    unsigned int len;
};

template<typename IntegerConversionStrategy>
struct IntegerConverter {
    using Self = IntegerConverter<IntegerConversionStrategy>;

    static constexpr auto PREFIX = IntegerConversionStrategy::PREFIX;

    static constexpr unsigned int MAX_UNSIGNED_LEN =
        IntegerConversionStrategy::unsigned_len(UINT64_MAX)
        + static_cast<unsigned int>(PREFIX.len);
    static constexpr unsigned int MAX_SIGNED_LEN =
        IntegerConversionStrategy::unsigned_len(-static_cast<size_t>(INT64_MIN)) + 1
        + static_cast<unsigned int>(PREFIX.len);

    static constexpr unsigned int MAX_LEN =
        ::utter::detail::max(MAX_UNSIGNED_LEN, MAX_SIGNED_LEN);

    /* Helpers, these are istantiated later with great care to avoid code bloat */

    UTTER_INLINE constexpr ConversionArgs process_flags(int flags) {
        // first 12 bits are reserved for fill count
        unsigned int fill = flags & 0xFFF;

        // cap filling at MAX_UNSIGNED_LEN (TODO: test exact max_unsigned_len when max_unsigned = max_signed)
        fill = ::utter::detail::min(fill, MAX_UNSIGNED_LEN);

        return {
            .wants_prefix = (flags & CONV_NO_PREFIX) == 0,
            .fill = fill,
        };
    }

    UTTER_INLINE constexpr size_t to_unsigned(size_t val) {
        return val;
    }

    UTTER_INLINE constexpr size_t to_unsigned(ssize_t val) {
        return val > 0 ? static_cast<size_t>(val) : (~static_cast<size_t>(val) + 1);
    }

    UTTER_INLINE constexpr unsigned int
    unsigned_len(size_t val, const ConversionArgs& args) {
        // start from unsigned len (depends on implementation)
        unsigned int len = val == 0 ? 1 : IntegerConversionStrategy::unsigned_len(val);

        // increase length if filling is requested
        // NOTE: fill is guaranteed to be capped correctly after process_flags
        len = ::utter::detail::max(args.fill, len);

        // increase length if prefix is requested
        len += static_cast<unsigned int>(PREFIX.len)
            * static_cast<unsigned int>(args.wants_prefix);

        return len;
    }

    UTTER_INLINE constexpr unsigned int sign_len(size_t val) {
        (void) val;

        return 0;
    }

    UTTER_INLINE constexpr unsigned int sign_len(ssize_t val) {
        return static_cast<unsigned int>(val < 0);
    }

    UTTER_INLINE constexpr char*
    unsigned_into(char* buf_end, size_t val, const ConversionArgs& args) {
        char* buf_start = IntegerConversionStrategy::unsigned_to_string(val, buf_end);

        // fill with zeroes if requested
        for (int fill =
                 static_cast<int>(args.fill) - static_cast<int>(buf_end - buf_start);
             fill > 0;
             fill--) {
            buf_start--;
            *buf_start = '0';
        }

        // add prefix if requested
        if (args.wants_prefix) {
            buf_start = ::utter::detail::small_inline_reverse_memcpy<PREFIX>(buf_start);
        }

        return buf_start;
    }

    UTTER_INLINE constexpr char* sign_into(char* buf_end, size_t val) {
        (void) val;

        return buf_end;
    }

    UTTER_INLINE constexpr char* sign_into(char* buf_end, ssize_t val) {
        if (val < 0) {
            --buf_end;
            *buf_end = '-';
        }

        return buf_end;
    }

    /* Actual functions, each one of these is constructed by blocks constituted of helpers above
    *  since each of these is possibly emitted as assembly the number must be kept small.
    */

    template<typename I>
    static constexpr ConversionResult into_impl(char* buf, I val, int flags) {
        const ConversionArgs args = Self::process_flags(flags);

        // get an unsigned representation
        const size_t unsigned_val = Self::to_unsigned(val);

        // calculate length to find the end of the buffer
        const unsigned int len =
            Self::unsigned_len(unsigned_val, args) + Self::sign_len(val);

        // calculate end of buffer
        char* const buf_end = buf + len;

        // write the unsigned number representation
        char* buf_start = Self::unsigned_into(buf_end, unsigned_val, args);

        // add minus sign if the value is negative
        buf_start = Self::sign_into(buf_start, val);

        return {.buf_end = buf_end, .len = len};
    }

    /* External facing API, instantiates a signed/unsigned pair of the functions above */

    template<typename I>
    UTTER_INLINE constexpr ConversionResult into(char* buf, I val, int flags) {
        if constexpr (std::is_unsigned_v<I>) {
            return Self::into_impl(buf, static_cast<size_t>(val), flags);
        } else {
            return Self::into_impl(buf, static_cast<ssize_t>(val), flags);
        }
    }

    template<typename I>
    UTTER_INLINE constexpr auto to_buffer(I val, int flags) {
        // todo. if constexpr -> calc len
        if constexpr (std::is_unsigned_v<I>) {
            Buffer<MAX_UNSIGNED_LEN> buf = {};
            const ConversionResult result =
                Self::into_impl(buf.data, static_cast<size_t>(val), flags);
            buf.len = result.len;
            return buf;
        } else {
            Buffer<MAX_SIGNED_LEN> buf = {};
            const ConversionResult result =
                Self::into_impl(buf.data, static_cast<ssize_t>(val), flags);
            buf.len = result.len;
            return buf;
        }
    }

    template<auto val, int flags>
    static consteval auto to_buffer() {
        constexpr auto buf = ::utter::detail::to_buffer_impl(val, flags);
        return Buffer<buf.len>{.len = buf.len, .data = buf.data};
    }
};

struct DecimalConversionStrategy {
    static constexpr Buffer<0> PREFIX = ::utter::to_buffer("");

    // 2^x has "power_of_two_digits_minus_one[x] - 1" digits in decimal
    // due to how __builtin_clz works 0 has a special val of 1
    static constexpr unsigned char POWER_OF_TWO_DIGITS_MINUS_ONE[65] = {
        // clang-format off

        // 0, 1, 2, 4, 8
        0, 0, 0, 0,

        // 16, 32, 64
        1, 1, 1,

        // 128, 256, 512
        2, 2, 2,

        // 1024, 2048, 4096, 8192
        3, 3, 3, 3,

        4, 4, 4,
        5, 5, 5,
        6, 6, 6, 6,

        7, 7, 7,
        8, 8, 8,
        9, 9, 9, 9,

        10, 10, 10,
        11, 11, 11,
        12, 12, 12, 12,

        13, 13, 13,
        14, 14, 14,
        15, 15, 15, 15,

        16, 16, 16,
        17, 17, 17,
        18, 18, 18, 18,

        19,

        // clang-format on
    };

    // 10^x = powers_of_ten[x]
    static constexpr size_t POWERS_OF_TEN[] = {
        1,
        10,
        100,
        1000,
        10000,
        100000,
        1000000,
        10000000,
        100000000,
        1000000000,
        10000000000,
        100000000000,
        1000000000000,
        10000000000000,
        100000000000000,
        1000000000000000,
        10000000000000000,
        100000000000000000,
        1000000000000000000U,
#if INTPTR_MAX > INT32_MAX
        10000000000000000000U,
#endif
    };

    // LUT of the first 100 numbers, see this as an array of 50 elements
    // with size of each element == 2 * sizeof(char)
    static constexpr char DECIMAL_LUT[] =
        "0001020304050607080910111213141516171819"
        "2021222324252627282930313233343536373839"
        "4041424344454647484950515253545556575859"
        "6061626364656667686970717273747576777879"
        "8081828384858687888990919293949596979899";

    UTTER_INLINE constexpr unsigned int unsigned_len(size_t val) {
        // inspired by https://stackoverflow.com/questions/25892665/performance-of-log10-function-returning-an-int
        // how does this work, by example:
        // - 75:
        // digits in binary = 7
        // guess[7] = 2
        // powers_of_ten[2] = 100
        // 75 < 100 -> returns 2
        // - 110:
        // digits in binary = 7
        // guess[7] = 2
        // powers_of_ten[2] = 100
        // 110 > 100 -> returns 3

        const unsigned int digits_in_binary = ::utter::detail::count_binary_digits(val);

        unsigned int digits = POWER_OF_TWO_DIGITS_MINUS_ONE[digits_in_binary];
        digits += static_cast<unsigned int>(val >= POWERS_OF_TEN[digits]);

        return digits;
    }

    UTTER_INLINE constexpr char* unsigned_to_string(size_t val, char* buf_end) {
        // unrolling of the case > 10000
        while (val >= 10000) {
            const size_t rem = val % 10000;

            val /= 10000;

            const size_t digit_pair1 = (rem / 100) << 1;
            const size_t digit_pair2 = (rem % 100) << 1;

            buf_end -= 2;
            buf_end[0] = DECIMAL_LUT[digit_pair2];
            buf_end[1] = DECIMAL_LUT[digit_pair2 + 1];

            buf_end -= 2;
            buf_end[0] = DECIMAL_LUT[digit_pair1];
            buf_end[1] = DECIMAL_LUT[digit_pair1 + 1];
        }

        if (val >= 100) {
            // the index on the lut above is given by:
            // - dividing by 100 to find "the cell"
            // - multiplying by two since each cell is made by two chars
            const size_t digit_pair = (val % 100) << 1;

            val /= 100;

            // decrement the current position since we start from the end of the buffer
            buf_end -= 2;
            buf_end[0] = DECIMAL_LUT[digit_pair];
            buf_end[1] = DECIMAL_LUT[digit_pair + 1];
        }

        if (val >= 10) {
            const unsigned int digit_pair = static_cast<unsigned int>(val) << 1;
            buf_end -= 2;
            buf_end[0] = DECIMAL_LUT[digit_pair];
            buf_end[1] = DECIMAL_LUT[digit_pair + 1];
        } else {
            buf_end--;
            *buf_end = static_cast<char>(val + '0');
        }

        return buf_end;
    }
};

struct HexadecimalConversionStrategy {
    static constexpr Buffer<2> PREFIX = ::utter::to_buffer("0x");

    static constexpr char HEX_LUT[] = "0123456789abcdef";

    UTTER_INLINE constexpr unsigned int unsigned_len(size_t val) {
        const unsigned int digits_in_binary = ::utter::detail::count_binary_digits(val);
        return digits_in_binary / 4
            + static_cast<unsigned int>((digits_in_binary % 4) != 0);
    }

    UTTER_INLINE constexpr char* unsigned_to_string(size_t val, char* buf_end) {
        while (val > 15) {
            buf_end--;
            *buf_end = HEX_LUT[val & 0xF];
            val >>= 4;
        }

        buf_end--;
        *buf_end = HEX_LUT[val & 0xF];

        return buf_end;
    }
};

struct BinaryConversionStrategy {
    static constexpr Buffer<2> PREFIX = ::utter::to_buffer("0b");

    UTTER_INLINE constexpr unsigned int unsigned_len(size_t val) {
        return ::utter::detail::count_binary_digits(val);
    }

    UTTER_INLINE constexpr char* unsigned_to_string(size_t val, char* buf_end) {
        while (val > 1) {
            buf_end--;
            *buf_end = static_cast<char>((val & 0x1) + '0');

            val >>= 1;
        }

        buf_end--;
        *buf_end = static_cast<char>((val & 0x1) + '0');

        return buf_end;
    }
};

} // namespace utter::detail

namespace utter {

using DEC = detail::DecimalConversionStrategy;
using HEX = detail::HexadecimalConversionStrategy;
using BIN = detail::BinaryConversionStrategy;

template<typename Strategy = DEC, typename I>
UTTER_INLINE constexpr auto as(I val, int flags = 0) {
    return ::utter::detail::IntegerConverter<Strategy>::to_buffer(val, flags);
}

template<typename Strategy = DEC, auto val, int flags = 0>
UTTER_INLINE constexpr auto as() {
    // only uses the memory needed to store the integer as string
    // instead of MAX_UNSIGNED_LEN/MAX_SIGNED_LEN
    return ::utter::detail::IntegerConverter<Strategy>::template to_buffer<val, flags>();
}

template<typename Strategy = DEC, typename I>
UTTER_INLINE constexpr auto as_into(char* buffer, I val, int flags = 0) {
    return ::utter::detail::IntegerConverter<Strategy>::into(buffer, val, flags);
}

template<typename I>
// avoid resolution conflict with the overload for a single char
    requires std::integral<I> && (!std::same_as<I, char>)
UTTER_INLINE constexpr auto to_buffer(I val) {
    return ::utter::as<DEC>(val);
}

} // namespace utter

/* Floating point conversion */

// TODO

/* External utilities */

namespace utter::detail {

template<typename T>
UTTER_INLINE constexpr void concat_impl(T&&) {
}

template<typename T1, typename T2, typename... Ts>
UTTER_INLINE constexpr void concat_impl(T1&& buf1, T2&& buf2, Ts&&... bufs) {
    if (std::is_constant_evaluated()) {
        for (size_t i = 0; i < buf2.len; i++) {
            buf1.data[buf1.len + i] = buf2.data[i];
        }
    } else {
        ::memcpy(buf1.data + buf1.len, buf2.data, buf2.len);
    }

    buf1.len += buf2.len;
    ::utter::detail::concat_impl(buf1, bufs...);
}

} // namespace utter::detail

namespace utter {

template<typename... Args>
UTTER_INLINE constexpr auto concat(Args&&... args) {
    constexpr size_t merged_buf_len =
        (std::remove_reference_t<decltype(::utter::to_buffer(args))>::SIZE + ... + 0);
    Buffer<merged_buf_len> merged_buf = {
        .len = 0,
        .data = {},
    };

    ::utter::detail::concat_impl(merged_buf, ::utter::to_buffer(args)...);

    return merged_buf;
}

template<size_t n>
UTTER_INLINE constexpr auto repeat(char chr) {
    Buffer<n> buf = {
        .len = n,
        .data = {},
    };
    if (std::is_constant_evaluated()) {
        for (size_t i = 0; i < n; i++) {
            buf.data[i] = chr;
        }
    } else {
        ::memset(buf.data, chr, n);
    }

    return buf;
}

} // namespace utter

/* IO */

namespace utter::detail {

template<typename T>
UTTER_INLINE constexpr iovec to_iovec(T&& buf) {
    return {
        .iov_base = const_cast<char*>(buf.data),
        .iov_len = buf.len,
    };
}

template<typename... Ts>
static ssize_t to(int file, Ts&&... bufs) {
    const iovec vecs[] = {
        ::utter::detail::to_iovec(bufs)...,
    };
    return ::writev(file, vecs, sizeof...(Ts));
}

} // namespace utter::detail

namespace utter {

static constexpr int STDOUT = 1;
static constexpr int STDERR = 2;

template<typename... Args>
UTTER_INLINE ssize_t to(int file, Args&&... args) {
    // need a wrapper to keep the buffers alive for the whole duration
    // of the function
    return ::utter::detail::to(file, ::utter::to_buffer(args)...);
}

} // namespace utter

#undef UTTER_INLINE

#endif
