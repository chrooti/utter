#!/bin/bash

main() {
    local cxx
    if [[ -z ${CXX+x} ]]; then
        cxx=/usr/bin/c++
    else
        cxx="${CXX}"
    fi

    echo "CXX = ${cxx}"

    local cxxflags=()
    local ldflags=()

    local debug_cxxflags=(-O1 -g -fno-optimize-sibling-calls -fno-omit-frame-pointer)

    if [[ ${SANITIZE} == "memory" ]]; then
        # shellcheck disable=SC2206
        cxxflags+=(
            ${debug_cxxflags[@]}
            -fsanitize=memory
            -fsanitize-memory-track-origins
            -fsanitize-recover=all
        )

        ldflags+=(
            -g
            -fsanitize=memory
            -fsanitize-recover=all
        )
    elif [[ ${SANITIZE} == "address" ]]; then
        # shellcheck disable=SC2054,SC2206
        cxxflags+=(
            ${debug_cxxflags[@]}
            -fsanitize=address,undefined,integer,nullability
            -fsanitize-address-use-after-scope
            -fsanitize-recover=all
        )

        # shellcheck disable=SC2054
        ldflags+=(
            -g
            -fsanitize=address,undefined,integer,nullability
            -fsanitize-recover=all
        )
    elif [[ ${DEBUG} == "1" ]]; then
        # shellcheck disable=SC2206
        cxxflags+=(${debug_cxxflags[@]})
    else
        cxxflags+=(-DNDEBUG)
    fi

    if [[ ${COVERAGE} == "1" ]]; then
        cxxflags+=(-fprofile-instr-generate -fcoverage-mapping)
        ldflags+=(-fprofile-instr-generate -fcoverage-mapping)
    fi

    if [[ -n ${CXXFLAGS+x} ]]; then
        # shellcheck disable=SC2206
        cxxflags+=(${CXXFLAGS[@]})
    fi

    echo "CXXFLAGS =" "${cxxflags[@]}"

    if [[ -n ${LDFLAGS+x} ]]; then
        # shellcheck disable=SC2206
        ldflags+=(${LDFLAGS[@]})
    fi

    echo "LDFLAGS =" "${ldflags[@]}"
}

main > config.ninja
