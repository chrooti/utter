#!/usr/bin/env bash

set -e -o pipefail
shopt -s failglob globstar

SOURCES=(utter.hpp tests.cpp)

LLVM_CONFIG="${LLVM_CONFIG:-llvm-config}"

LLVM_PROFILE_FILE=out/tests.profraw
LLVM_PROFDATA_FILE=out/tests.profdata

llvm_tool() {
	"$("$LLVM_CONFIG" --bindir)/${1}" "${@:2}"
}

task_setup() {
	ninja -t compdb cc > compile_commands.json
}

task_clean() {
	rm -rf out
	rm -f compile_commands.json
}

task_format() {
	export -f llvm_tool

	find "${SOURCES[@]}" \
		-type f \
		\( -name '*.hpp' -o -name '*.cpp' \) \
		-exec bash -c 'llvm_tool clang-format --style=file -i "$@"' _ {} +

	find "${SOURCES[@]}" \
		-type f \
		-name '*.cpp' \
		-exec bash -c 'llvm_tool clang-tidy --fix "$@"' _ {} +
}

task_lint() {
	export -f llvm_tool

	find "${SOURCES[@]}" \
		-type f \
		\( -name '*.hpp' -o -name '*.cpp' \) \
		-exec bash -c 'llvm_tool clang-format --style=file --dry-run --Werror "$@"' _ {} +

	find "${SOURCES[@]}" \
		-type f \
		-name '*.cpp' \
		-exec bash -c 'llvm_tool clang-tidy "$@"' _ {} +
}

task_test() {
	ninja

	ASAN_OPTIONS=check_initialization_order=1 \
		LLVM_PROFILE_FILE="${LLVM_PROFILE_FILE}" \
		out/tests
}

task_coverage() {
	llvm_tool llvm-profdata merge \
		-sparse "${LLVM_PROFILE_FILE}" \
		-o "${LLVM_PROFDATA_FILE}"

	llvm_tool llvm-cov show out/tests \
		-instr-profile="${LLVM_PROFDATA_FILE}" \
		-show-branches=count \
		-show-expansions
}

main() {
	task="$1"
	shift

	"task_${task}" "$@"
}

main "$@"
