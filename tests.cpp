#include "utter.hpp"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace utter::literals;

template<typename... Ts>
struct Tuple;

template<typename T1>
struct Tuple<T1> {
    T1 item;
};

template<typename T1, typename T2, typename... Ts>
struct Tuple<T1, T2, Ts...> {
    T1 item;
    Tuple<T2, Ts...> next;
};

namespace tuple {

template<typename T1>
static inline constexpr Tuple<T1> make(T1 item1) {
    return {.item = item1};
}

template<typename T1, typename T2, typename... Ts>
static inline constexpr Tuple<T1, T2, Ts...> make(T1 item1, T2 item2, Ts... items) {
    return {
        .item = item1,
        .next = ::tuple::make(item2, items...),
    };
}

} // namespace tuple

static int fail_if_constexpr_impl() {
    return 0;
}

#define FAIL_IF_CONSTEXPR_OR(CONDITION, MESSAGE) \
    if (std::is_constant_evaluated()) { \
        if (CONDITION) { \
            fail_if_constexpr_impl(); \
        } \
    } \
    if (CONDITION)

static constexpr size_t string_length(const char* str) {
    size_t len = 0;
    while (*str != '\0') {
        ++str;
        len += 1;
    }

    return len;
}

template<typename T>
static constexpr int assert_values_equal(const T& val1, const T& val2) {
    return 1 - (val1 == val2);
}

template<size_t buf_size>
static constexpr int assert_buffer_equal(
    const utter::Buffer<buf_size>& buf,
    const utter::BufferView& buf_view
) {
    FAIL_IF_CONSTEXPR_OR(buf.len != buf_view.len, "String length differs") {
        utter::to(
            utter::STDERR,
            S("actual   = "),
            buf,
            S("\nexpected = "),
            buf_view,
            S("\n\n")
        );
        return 1;
    }

    for (size_t i = 0; i < buf.len; i++) {
        FAIL_IF_CONSTEXPR_OR(buf.data[i] != buf_view.data[i], "String length differs") {
            utter::to(
                utter::STDERR,
                S("actual   = "),
                buf,
                S("\nexpected = "),
                buf_view,
                S("\n\n")
            );
            return 1;
        }
    }

    return 0;
}

template<typename Value>
struct IntegerConversionFixture {
    Value val;
    int fill_count;

    utter::BufferView str;
    utter::BufferView str_filled;
    utter::BufferView str_no_prefix;
};

template<typename Strategy>
struct IntegerConversionTestCase {
    static constexpr bool IS_CONSTEXPR = true;

    template<typename Value>
    static constexpr int execute(const IntegerConversionFixture<Value>& fixture) {
        return (
            assert_buffer_equal(utter::as<Strategy>(fixture.val), fixture.str)
            + assert_buffer_equal(
                utter::as<Strategy>(fixture.val, fixture.fill_count),
                fixture.str_filled
            )
            + assert_buffer_equal(
                utter::as<Strategy>(fixture.val, utter::CONV_NO_PREFIX),
                fixture.str_no_prefix
            )
        );
    }
};

struct DecimalConversionTestCase : IntegerConversionTestCase<utter::DEC> {
    static constexpr auto FIXTURES = tuple::make(
        IntegerConversionFixture{
            .val = 16,
            .fill_count = 3,
            .str = utter::to_buffer_view("16"),
            .str_filled = utter::to_buffer_view("016"),
            .str_no_prefix = utter::to_buffer_view("16"),
        },
        IntegerConversionFixture{
            .val = 0,
            .fill_count = 3,
            .str = utter::to_buffer_view("0"),
            .str_filled = utter::to_buffer_view("000"),
            .str_no_prefix = utter::to_buffer_view("0"),
        },
        IntegerConversionFixture{
            .val = INT64_MIN,
            .fill_count = 3,
            .str = utter::to_buffer_view("-9223372036854775808"),
            .str_filled = utter::to_buffer_view("-9223372036854775808"),
            .str_no_prefix = utter::to_buffer_view("-9223372036854775808"),
        },
        IntegerConversionFixture{
            .val = UINT64_MAX,
            .fill_count = 3,
            .str = utter::to_buffer_view("18446744073709551615"),
            .str_filled = utter::to_buffer_view("18446744073709551615"),
            .str_no_prefix = utter::to_buffer_view("18446744073709551615"),
        }
    );
};

struct HexadecimalConversionTestCase : IntegerConversionTestCase<utter::HEX> {
    static constexpr auto FIXTURES = tuple::make(
        IntegerConversionFixture{
            .val = 16,
            .fill_count = 3,
            .str = utter::to_buffer_view("0x10"),
            .str_filled = utter::to_buffer_view("0x010"),
            .str_no_prefix = utter::to_buffer_view("10"),
        },
        IntegerConversionFixture{
            .val = 0,
            .fill_count = 3,
            .str = utter::to_buffer_view("0x0"),
            .str_filled = utter::to_buffer_view("0x000"),
            .str_no_prefix = utter::to_buffer_view("0"),
        },
        IntegerConversionFixture{
            .val = INT64_MIN,
            .fill_count = 3,
            .str = utter::to_buffer_view("-0x8000000000000000"),
            .str_filled = utter::to_buffer_view("-0x8000000000000000"),
            .str_no_prefix = utter::to_buffer_view("-8000000000000000"),
        },
        IntegerConversionFixture{
            .val = UINT64_MAX,
            .fill_count = 3,
            .str = utter::to_buffer_view("0xffffffffffffffff"),
            .str_filled = utter::to_buffer_view("0xffffffffffffffff"),
            .str_no_prefix = utter::to_buffer_view("ffffffffffffffff"),
        }
    );
};

struct BinaryConversionTestCase : IntegerConversionTestCase<utter::BIN> {
    static constexpr auto FIXTURES = tuple::make(
        // binaries
        IntegerConversionFixture{
            .val = 16,
            .fill_count = 6,
            .str = utter::to_buffer_view("0b10000"),
            .str_filled = utter::to_buffer_view("0b010000"),
            .str_no_prefix = utter::to_buffer_view("10000"),
        },
        IntegerConversionFixture{
            .val = 0,
            .fill_count = 6,
            .str = utter::to_buffer_view("0b0"),
            .str_filled = utter::to_buffer_view("0b000000"),
            .str_no_prefix = utter::to_buffer_view("0"),
        },
        IntegerConversionFixture{
            .val = INT64_MIN,
            .fill_count = 6,
            .str = utter::to_buffer_view(
                "-0b1000000000000000000000000000000000000000000000000000000000000000"
            ),
            .str_filled = utter::to_buffer_view(
                "-0b1000000000000000000000000000000000000000000000000000000000000000"
            ),
            .str_no_prefix = utter::to_buffer_view(
                "-1000000000000000000000000000000000000000000000000000000000000000"
            ),
        },
        IntegerConversionFixture{
            .val = UINT64_MAX,
            .fill_count = 6,
            .str = utter::to_buffer_view(
                "0b1111111111111111111111111111111111111111111111111111111111111111"
            ),
            .str_filled = utter::to_buffer_view(
                "0b1111111111111111111111111111111111111111111111111111111111111111"
            ),
            .str_no_prefix = utter::to_buffer_view(
                "1111111111111111111111111111111111111111111111111111111111111111"
            ),
        }
    );
};

struct ConcatTestCase {
    static constexpr bool IS_CONSTEXPR = true;

    static constexpr int execute() {
        return assert_buffer_equal(
            S(utter::concat(S("test concatenation of "), 3, S(" bufs"))),
            utter::to_buffer_view("test concatenation of 3 bufs")
        );
    }
};

struct ImplicitDecimalConversionTestCase {
    static constexpr bool IS_CONSTEXPR = true;

    static constexpr int execute() {
        return assert_buffer_equal(S(16), utter::to_buffer_view("16"));
    }
};

struct DecimalLiteralTestCase {
    static constexpr bool IS_CONSTEXPR = true;

    static constexpr int execute() {
        return assert_buffer_equal(S(16_str), utter::to_buffer_view("16"));
    }
};

struct DecimalConversionOverfillTestCase {
    static constexpr bool IS_CONSTEXPR = true;

    static constexpr int execute() {
        return assert_buffer_equal(
            utter::as<utter::DEC>(16, 99),
            utter::to_buffer_view("00000000000000000016")
        );
    }
};

struct RepeatTestCase {
    static constexpr bool IS_CONSTEXPR = true;

    static constexpr int execute() {
        return assert_buffer_equal(utter::repeat<5>('a'), utter::to_buffer_view("aaaaa"));
    }
};

struct IoTestCase {
    static constexpr bool IS_CONSTEXPR = false;

    static int execute() {
        return assert_values_equal(
                   utter::to(
                       utter::STDOUT,
                       S("test"),
                       S(" printing"),
                       S(" works"),
                       S("\n")
                   ),
                   0L
               )
            + assert_values_equal(
                   utter::to(
                       utter::STDOUT,
                       utter::to_buffer_view("test printing works\n")
                   ),
                   0L
            );
    }
};

template<typename T>
T& taint(T& a) {
    asm volatile("" ::: "memory");
    return a;
}

template<auto... expr>
static constexpr void enforce_constexpr() {
}

template<template<typename...> typename Template, typename... Ts>
void is_specialization_of_impl(const Template<Ts...>&);

template<typename T, template<typename...> typename Template>
concept is_specialization_of =
    requires (const T& t) { is_specialization_of_impl<Template>(t); };

template<typename T>
concept TestCaseSimple = requires {
    T::IS_CONSTEXPR;
    T::execute();
};

template<typename T>
concept TestCaseWithFixtures = requires {
    T::IS_CONSTEXPR;
    { T::FIXTURES } -> is_specialization_of<Tuple>;
    T::execute(T::FIXTURES.item);
};

template<typename TestCase>
struct TestCaseRunner;

template<TestCaseSimple TestCase>
struct TestCaseRunner<TestCase> {
    static void execute() {
        if constexpr (TestCase::IS_CONSTEXPR) {
            enforce_constexpr<TestCase::execute()>();
        }

        TestCase::execute();
    }
};

template<TestCaseWithFixtures TestCase>
struct TestCaseRunner<TestCase> {
    template<typename F1>
    static constexpr int do_execute(const Tuple<F1>& fixtures) {
        TestCase::execute(fixtures.item);
        return 0;
    }

    template<typename F1, typename F2, typename... Fs>
    static constexpr int do_execute(const Tuple<F1, F2, Fs...>& fixtures) {
        TestCase::execute(fixtures.item);
        return do_execute(fixtures.next);
    }

    static void execute() {
        if constexpr (TestCase::IS_CONSTEXPR) {
            enforce_constexpr<do_execute(TestCase::FIXTURES)>();
        }

        do_execute(taint(TestCase::FIXTURES));
    }
};

int main(int argc, char** argv) {
    (void) argc;
    (void) argv;

    TestCaseRunner<DecimalConversionTestCase>::execute();
    TestCaseRunner<HexadecimalConversionTestCase>::execute();
    TestCaseRunner<BinaryConversionTestCase>::execute();
    TestCaseRunner<ConcatTestCase>::execute();
    TestCaseRunner<ImplicitDecimalConversionTestCase>::execute();
    TestCaseRunner<DecimalLiteralTestCase>::execute();
    TestCaseRunner<DecimalConversionOverfillTestCase>::execute();
    TestCaseRunner<RepeatTestCase>::execute();
    TestCaseRunner<IoTestCase>::execute();
}
